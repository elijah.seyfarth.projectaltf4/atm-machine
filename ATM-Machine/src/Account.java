/**
 * 
 * Klasse dient der administrativen Accountverwaltung 
 * 
 * @author Marco
 *
 */

public class Account implements Comparable<Object> {
	private int accountNumber;
	private String pin;
	private double totalBalance;
	private boolean admin;
	private String username;
	
	/**
	* Konstruktor dient der Initalisierung des Kunden mit vorhandener Kontonummer.
	*
	* @param accountNumber Kontonummer
	* @param username Benutzername
	* @param thePIN PIN-Nummer
	* @param theTotalBalance Kontostand
	* @param isadmin Administrative Rechte
	*/
	
	public Account(String accountNumber, String username, String thePIN, double theTotalBalance, boolean isadmin) {
		setAccountNumber(accountNumber);
		setUsername(username);
		setPin(thePIN);
		setTotalBalance(theTotalBalance);
		setAdmin(isadmin);

	}
	
	/**
	* Konstruktor dient der Initalisierung des Kunden sowie der Zuteilung einer neuen
	* Kontonummer.
	* 
	* @param username Benutzername
	* @param thePIN PIN-Nummer
	* @param theTotalBalance Kontostand
	* @param isadmin Administrative Rechte
	*/

	public Account(String username, String thePIN, double theTotalBalance, boolean isadmin) {
		setAccountNumber(FileManager.getNewAccountNumber(isadmin));
		setUsername(username);
		setPin(thePIN);
		setTotalBalance(theTotalBalance);
		setAdmin(isadmin);
	}
	
	/**
	* Methode dient der Ueberpruefung der korrekten PIN-Eingabe.
	* 
	* @param userPIN Nutzereingabe
	* @return Ergebnis des Vergleichs
	*/
	
	public boolean validatePIN(String userPIN) {
		if (userPIN == getPin())
			return true;
		else
			return false;
	}
	
	/**
	* Dient der Darstellung des Kontostandes.
	* @return 	Kontostand
	*/

	public double getTotalBalance() {
		return totalBalance;
	}

	/**
	* Methode vergibt Kredit an Nutzerkonto.
	* @param amount Stellt Kredithoehe dar
	*/

	public void credit(double amount) {
		setTotalBalance(getTotalBalance() + amount);
	}
	
	/**
	* Verminderung des Kontostandes durch Lastschriftverfahren.
	* @param amount		Hoehe der Lastschrift
	*/

	public void debit(double amount) {
		setTotalBalance(getTotalBalance() - amount);
	}
	
	/**
	* @return	Rueckgabe des Benutzernamen.
	*/

	public String getUsername() {
		return username;
	}

	/**
	* @param username	Anpassung des Benutzernamen.
	*/
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	/**
	* @return	Rueckgabe der PIN-Nummer.
	*/

	public String getPin() {
		return pin;
	}
	
	/**
	* @param pin	Vergabe der PIN-Nummer
	*/

	public void setPin(String pin) {
		this.pin = pin;
	}
	
	/**
	 * @param totalBalance	Eingabe des aktuellen Kontostandes.
	 *
	 */

	public void setTotalBalance(double totalBalance) {
		this.totalBalance = totalBalance;
	}

	/**
	* @return	Gibt Abfrageergebnis administrativer Berechtigung wieder
	*/
	
	public boolean isAdmin() {
		return admin;
	}
	
	/**
	* @param admin		Legt administrative Berechtigung fest
	*/

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	/** 
	* @return	Rueckgabe der Kontonummer
	*/
	
	public String getAccountNumber() {
		return Integer.toString(this.accountNumber);
	}
	
	/**
	 * Umwandlung der Kontonummer vom Datentyp String in Integer-Wert
	 * 
	 * @param accountNumber Kontonummer
	 */

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = Integer.parseInt(accountNumber);
	}
	
	/**
	 * @return Kontonummer
	 */

	public int getAccountNumberAsInt() {
		return this.accountNumber;

	}
	/**
	 * Methode listet die bestehenden Kundenkonten anhand ihrer Kontonummer auf.
	 */
	@Override
	public int compareTo(Object o) {

		Account acc = (Account) o;

		if (this.accountNumber < acc.accountNumber) {
			return -1;
		} else if (this.accountNumber > acc.accountNumber) {
			return 1;
		}
		return 0;
	}
}