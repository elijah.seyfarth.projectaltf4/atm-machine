import java.text.DecimalFormat;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

/**
 * Diese Klasse repräsentiert die Nutzertabelle der Admin-Sicht
 * 
 * @author Elijah Seyfarth
 *
 */
public class Table {

	static JTable table;
	static Object[][] tableData;
	static DefaultTableModel model;

	/**
	 * Der Konstruktor initialisiert die Tabelle mit den Accounts
	 */
	public Table() {

		tableData = new Object[BankDatabase.accounts.keySet().size()][5];
		int index = 0;
		for (String key : BankDatabase.accounts.keySet()) {
			Account acc = BankDatabase.accounts.get(key);
			tableData[index][0] = key;
			tableData[index][1] = acc.getPin();
			tableData[index][2] = acc.getUsername();
			tableData[index][3] = new DecimalFormat("#,##0.00").format(acc.getTotalBalance());
			tableData[index][4] = acc.isAdmin();
			index++;
		}

		model = new DefaultTableModel(tableData, Translation.columnNames());
		table = new JTable(model) {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		ListSelectionModel listModel = table.getSelectionModel();
		listModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}
}
