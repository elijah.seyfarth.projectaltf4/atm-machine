
/**
 * Diese Aufz�hlung repr�sentiert alle m�glichen Transaktionen sowie
 * Admin-Operationen
 * 
 * @author Elijah Seyfarth
 *
 */
public enum LogTypes {
	DEPOSIT, WITHDRAWAL, TRANSFER, ADD, DELETE;

	/**
	 * Diese Methode gibt f�r den jeweiligen Typ eine String-Repr�sentation zur�ck
	 * 
	 * @return String-Repr�sentation
	 */
	@Override
	public String toString() {
		switch (this) {
		case ADD:
			return "Add Account";
		case DELETE:
			return "Delete Account";
		case DEPOSIT:
			return "menu4";
		case TRANSFER:
			return "menu5";
		case WITHDRAWAL:
			return "menu3";
		default:
			return "";
		}
	}
}
