/**
 * 
 * Klasse dient der Berechnung der verfuegbaren Geldmittel des Bankautomaten.
 * 
 * @author Marco
 *
 */

public class CashDispenser {

	private final static int INITIAL_COUNT = 4000; // 20.000 �
	private int count;

	public CashDispenser() {
		count = INITIAL_COUNT;
	}

	/**
	 * Methode dient der Aufspaltung von Geldentnahmen in Waehrungseinheiten von (5).
	 * 
	 * @param amount Abzuhebende Geldmenge
	 */
	public void dispenseCash(int amount) {
		int billsRequired = amount / 5;
		count -= billsRequired;
	}
	
	/**
	 * Methode prueft die zur Verfuegung stehende Menge Bargeld im Bankautomaten.
	 * 
	 * @param amount Abzuhebende Geldmenge
	 * @return Verfuegbare Geldmenge ausreichend oder nicht ausreichend
	 */

	public boolean isSufficientCashAvailable(int amount) {
		int billsRequired = amount / 5;

		if (count >= billsRequired)
			return true;
		else
			return false;
	}
}