/**
 * Diese Klasse ist das Schema f�r die Transaktionen
 * 
 * @author Elijah Seyfarth
 *
 */
public abstract class Transaction {

	private String accountNumber;
	protected Screen screen;
	private BankDatabase bankDatabase;
	protected ATM atm;

	/**
	 * 
	 * @param userAccountNumber Kontonummer
	 * @param atmScreen         Fensterinstanz
	 * @param atmBankDatabase   Datenbankinstanz
	 * @param theATM            ATM-Instanz
	 */
	public Transaction(String userAccountNumber, Screen atmScreen, BankDatabase atmBankDatabase, ATM theATM) {

		accountNumber = userAccountNumber;
		screen = atmScreen;
		bankDatabase = atmBankDatabase;
		atm = theATM;
	}

	/**
	 * 
	 * @return Kontonummer
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * 
	 * @return Fensterinstanz
	 */
	public Screen getScreen() {
		return screen;
	}

	/**
	 * 
	 * @return Datenbankinstanz
	 */
	public BankDatabase getBankDatabase() {
		return bankDatabase;
	}

	/**
	 * 
	 * @return ATM-Instanz
	 */
	public ATM getAtm() {
		return atm;
	}

	/**
	 * Methode zum Start der jeweiligen Transaktion
	 */
	public abstract void execute();
}