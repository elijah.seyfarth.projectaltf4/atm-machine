import java.text.DecimalFormat;

/**
 * Diese Klasse untersucht den Kontostand
 * 
 * @author Simon
 *
 */

public class BalanceInquiry extends Transaction {
	
	/**
	 * Der Konstruktor ruft den Superkonstruktor mit den eingegebenen Werten auf
	 * 
	 * @param userAccountNumber	Kontonummer
	 * @param atmScreen			Fensterinstanz
	 * @param atmBankDatabase	Datenbankinstanz
	 * @param atm				ATM-Instanz
	 */
	public BalanceInquiry(String userAccountNumber, Screen atmScreen, BankDatabase atmBankDatabase, ATM atm) {

		super(userAccountNumber, atmScreen, atmBankDatabase, atm);
	}
	
	/**
	 * Methode zum Start der Kontostandsuntersuchung
	 */
	@Override
	public void execute() {

		BankDatabase bankDatabase = getBankDatabase();
		Screen screen = getScreen();
		double totalBalance = bankDatabase.getTotalBalance(getAccountNumber());
		screen.createBalanceGUI();
		screen.label2.setText(Translation.getText("balance2") + new DecimalFormat("#,##0.00").format(totalBalance));
		screen.outputfield1.append(UserLog.getCustomerLogs(getAccountNumber()));
		screen.outputfield1.setCaretPosition(0);
		screen.mainFrame.revalidate();
	}
}