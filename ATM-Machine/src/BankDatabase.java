import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

import javax.swing.JOptionPane;

/**
 * Diese Klasse verwaltet die Datenbank des Geldautomaten
 * 
 * @author Elijah Seyfarth
 *
 */
public class BankDatabase {

	static LinkedHashMap<String, Account> accounts = new LinkedHashMap<String, Account>();

	/**
	 * Der Konstruktor initialisiert die Datenbank
	 */
	public BankDatabase() {

		Translation.fill();
		try {
			loadAllAccounts();
		} catch (Exception e) {
			initializeDatabase();
		}
	}

	/**
	 * Diese Methode pr�ft, ob ein Eintrag der angegebenen Accountnummer zusammen
	 * mit der angegebenen PIN in der Datenbank existiert
	 * 
	 * @param accNr Kontonummer
	 * @param pin   PIN
	 * @return Ergebnis der Authentifizierung
	 */
	public int authenticateUser(String accNr, String pin) {

		try {
			Integer.valueOf(accNr);
		} catch (NumberFormatException nfe) {
			return 0;
		}
		if (accounts.containsKey(accNr)) {
			if (accounts.get(accNr).getPin().equals(pin)) {
				return 2;
			} else {
				return 1;
			}
		} else {
			return 0;
		}
	}

	/**
	 * Diese Methode aktualisiert die Nutzerdaten eines ausgew�hlten Nutzers
	 * 
	 * @param accNr Kontonummer
	 */
	public void loadUser(String accNr) {

		Account acc = FileManager.refreshAccountData(accNr);
		accounts.put(accNr, acc);
	}

	/**
	 * Diese Methode l�dt alle Nutzerdaten aus der permanenten Datenbank in eine
	 * tempor�re Repr�sentation
	 */
	public void loadAllAccounts() {

		accounts = new LinkedHashMap<String, Account>();
		List<Account> allUserAccounts = FileManager.getAccountList();
		Collections.sort(allUserAccounts);
		allUserAccounts.forEach(account -> accounts.put(account.getAccountNumber(), account));
	}

	/**
	 * Diese Methode initialisiert eine v�llig leere Datenbank mit einem
	 * Admin-Account
	 */
	public void initializeDatabase() {

		String message = "Files of the Database couldn't be found. Should the Database be reset?";
		int del = JOptionPane.showConfirmDialog(null, message, null, JOptionPane.YES_NO_OPTION);
		if (del == 0) {
			Account admin = new Account("99000001", "admin0", "00000", 0, true);
			accounts.put("99000001", admin);
			FileManager.initializeUserFile(admin);
			FileManager.initializeLogFiles();
		} else {
			System.exit(0);
		}
	}

	/**
	 * Diese Methode sorgt f�r eine Erh�hung des Kontostandes der angegebenen
	 * Kontonummer um den angegebenen Betrag
	 * 
	 * @param userAccountNumber Kontonummmer
	 * @param amount            Betrag
	 */
	public void credit(String userAccountNumber, double amount) {
		FileManager.modifyBalance(userAccountNumber, amount);
		getAccount(userAccountNumber).credit(amount);
	}

	/**
	 * Diese Methode sorgt f�r einen Abzug des angegebenen Betrags vom Konto der
	 * angegebenen Kontonummer
	 * 
	 * @param userAccountNumber Kontonummer
	 * @param amount            Betrag
	 */
	public void debit(String userAccountNumber, double amount) {
		FileManager.modifyBalance(userAccountNumber, -amount);
		getAccount(userAccountNumber).debit(amount);
	}

	/**
	 * Diese Methode sorgt f�r das Hinzuf�gen eines neuen Accounts
	 * 
	 * @param name    Nutzername
	 * @param balance Kontostand
	 * @param pin     PIN
	 * @param admin   Adminrechte
	 */
	public void addAccount(String name, int balance, String pin, boolean admin) {

		try {
			if (pin.matches("\\d{5}")) {
				Integer.parseInt(pin);
				Account acc = new Account(name, pin, balance, admin);
				Object[] obj = { acc.getAccountNumber(), pin, name, new DecimalFormat("#,##0.00").format(balance),
						admin };
				accounts.put(acc.getAccountNumber(), acc);
				if (admin) {
					Table.model.addRow(obj);
				} else {
					Table.model.insertRow(getPosFrom(acc.getAccountNumber()), obj);
				}

				FileManager.addAccountInJsonFile(acc, admin);
				FileManager.logAdminOperation(acc.getAccountNumber(), LogTypes.ADD);
			} else {
				throw new NumberFormatException();
			}

		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, Translation.getText("adminErr1"));
		}
	}

	/**
	 * Diese Methode sorgt f�r die L�schung eines bestehenden Accounts
	 * 
	 * @param index Position
	 */
	public void deleteAccount(int index) {

		String accNr = getNrFrom(index);
		FileManager.deleteAccountFromJsonFile(accNr);
		FileManager.logAdminOperation(accNr, LogTypes.DELETE);
		accounts.remove(accNr);
		Table.model.removeRow(index);
	}

	/**
	 * Diese Methode sortiert die Accounts nach ihren Accountnummern und gibt eine
	 * sortierte Liste zur�ck
	 * 
	 * @return Accountliste
	 */
	public List<Account> sortAccounts() {

		List<Account> list = new ArrayList<Account>(accounts.values());
		Collections.sort(list);
		list.forEach((account -> accounts.put(account.getAccountNumber(), account)));
		return list;
	}

	/**
	 * Diese Methode gibt die Accountnummer des Accounts an der angegebenen Position
	 * zur�ck
	 * 
	 * @param index Position
	 * @return Kontonummer
	 */
	public String getNrFrom(int index) {

		return sortAccounts().get(index).getAccountNumber();
	}

	/**
	 * Diese Methode gibt die Position des Accounts mit der angegebenen Kontonummer
	 * zur�ck
	 * 
	 * @param accNr Kontonummer
	 * @return Position
	 */
	public int getPosFrom(String accNr) {

		return sortAccounts().indexOf(accounts.get(accNr));
	}

	/**
	 * 
	 * @param accountnumber Kontonummer
	 * @return Account
	 */
	public static Account getAccount(String accountnumber) {

		return accounts.get(accountnumber);
	}

	/**
	 * 
	 * @param userAccountNumber Kontonummer
	 * @return Kontostand
	 */
	public double getTotalBalance(String userAccountNumber) {
		return getAccount(userAccountNumber).getTotalBalance();
	}

	/**
	 * 
	 * @param userAccountNumber Kontonummer
	 * @return Adminrechte
	 */
	public boolean isAdmin(String userAccountNumber) {
		return accounts.get(userAccountNumber).isAdmin();
	}
}