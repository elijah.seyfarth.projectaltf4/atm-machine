import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

/**
 * Die Methode fuer die Abhebung
 * 
 * @author Simon
 *
 */

public class Withdrawal extends Transaction {

	private CashDispenser cashDispenser;
	
	/**
	 * Der Konstruktor ruft den Superkonstruktor mit den eingegebenen Werten auf und berechnet
	 * die Geldmittel des Automaten
	 * 
	 * @param userAccountNumber	Kontonummer
	 * @param atmScreen			Fensterinstanz
	 * @param atmBankDatabase	Datenbankinstanz
	 * @param atmCashDispenser	CashDispenser-Instanz
	 * @param atm				ATM-Instanz
	 */
	public Withdrawal(String userAccountNumber, Screen atmScreen, BankDatabase atmBankDatabase,
			CashDispenser atmCashDispenser, ATM atm) {

		super(userAccountNumber, atmScreen, atmBankDatabase, atm);
		cashDispenser = atmCashDispenser;
	}
	
	/**
	 * Methode zum Start der Auszahlung
	 */
	@Override
	public void execute() {
		displayMenuOfAmounts();
	}
	
	/**
	 * Diese Methode fuehrt die Auszahlung aus
	 * 
	 * @param amount Die abzuhebende Menge
	 */
	public void transaction(int amount) {

		BankDatabase bankDatabase = getBankDatabase();
		Screen screen = getScreen();

		if (cashDispenser.isSufficientCashAvailable(amount)) {

			bankDatabase.debit(getAccountNumber(), amount);
			FileManager.logTransaction(getAccountNumber(), getAccountNumber(), LogTypes.WITHDRAWAL, -amount);
			cashDispenser.dispenseCash(amount);
			screen.label2.setText(Translation.getText("withdrawConf"));
		} else
			screen.label2.setText(Translation.getText("insufficient2"));
	}
	
	/**
	 * Diese Methode erstellt die Ansicht f�r die Abhebung
	 */
	private void displayMenuOfAmounts() {

		Screen screen = getScreen();
		screen.createWithdrawGUI();
		Withdraw check = new Withdraw();
		screen.addActionListeners(check);
		screen.mainFrame.revalidate();
	}
	
	/**
	 * Diese innere Klasse stellt den ActionListener fuer die Abhebung zur Verfuegung
	 * @author Simon
	 *
	 */
	private class Withdraw implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			JButton b = (JButton) e.getSource();
			if (b.getName() != null) {
				transaction(Integer.parseInt(b.getName()));
			}
		}
	}

}