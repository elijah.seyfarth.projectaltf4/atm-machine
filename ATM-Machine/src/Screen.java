import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.plaf.synth.SynthLookAndFeel;

/**
 * Diese Klasse ist f�r die Benutzeroberfl�che zust�ndig
 * 
 * @author Elijah Seyfarth
 *
 */
public class Screen {

	public JFrame mainFrame;
	public JPanel background;
	public JPanel top;
	public JPanel middle;
	public JPanel body;
	public JTextField inputfield1;
	public JTextField inputfield2;
	public JTextField inputfield3;
	public JTextField inputfield4;
	public JPasswordField password;
	public JTextArea outputfield1;
	public JCheckBox checkBox;
	public JLabel label;
	public JLabel label2;
	public JLabel label3;
	public JLabel label4;
	public JLabel label5;
	public JLabel label6;
	public JLabel label7;
	public JLabel label8;
	public JLabel label9;
	public JButton b1, b2, b3, b4, b5, b6, b7;
	public JButton enter;
	public JButton exit;
	public JToggleButton language;
	GridBagLayout gridBag = new GridBagLayout();
	public int activeFrame = 0;

	/**
	 * Diese Methode initialisiert das Fenster
	 */
	public void initialize() {

		if (mainFrame == null) {
			setLookAndFeel();
			mainFrame = new JFrame("ATM");
			mainFrame.setLayout(new BorderLayout());
		} else {
			mainFrame.getContentPane().removeAll();
		}
	}

	/**
	 * Diese Methode �bertr�gt den Fokus von einer Komponente auf eine andere
	 * 
	 * @param to   Zielkomponente
	 * @param from Startkomponente
	 */
	public void switchComponent(JComponent to, JComponent from) {

		JTextField field1 = (JTextField) to;
		JTextField field2 = (JTextField) from;

		if (field1 != null) {
			field1.setEditable(true);
			field1.setFocusable(true);
			field1.requestFocus();
			field1.setBorder(new LineBorder(Color.BLACK, 2));
		}
		if (field2 != null) {
			field2.setEditable(false);
			field2.setFocusable(false);
			field2.setBorder(new LineBorder(Color.LIGHT_GRAY, 2));
		}

	}

	/**
	 * Diese Methode leert alle Eingabefelder
	 */
	public void clearFields() {

		try {
			inputfield1.setText("");
			inputfield2.setText("");
			inputfield3.setText("");
			inputfield4.setText("");

		} catch (NullPointerException e) {
		}
		try {
			password.setText("");
		} catch (NullPointerException e) {
		}
	}

	/**
	 * Diese Methode f�gt den angegebenen Actionlistener allen Schaltfl�chen hinzu
	 * 
	 * @param l Actionlistener
	 */
	public void addActionListeners(ActionListener l) {

		try {
			b1.addActionListener(l);
			b2.addActionListener(l);
			b3.addActionListener(l);
			b4.addActionListener(l);
			b5.addActionListener(l);
			b6.addActionListener(l);
			b7.addActionListener(l);

		} catch (NullPointerException e) {
		}
		if (exit != null) {
			exit.addActionListener(l);
		}
		if (enter != null) {
			enter.addActionListener(l);
		}
	}

	/**
	 * Diese Methode f�gt den Admin-Log der Admin-Sicht hinzu
	 */
	public void showAdminLog() {
		outputfield1.setText("");
		outputfield1.append(AdminLog.getAdminLog());
		outputfield1.setCaretPosition(0);
	}

	/**
	 * Diese Methode f�gt alle visuellen Komponenten der anf�nglichen Sprachauswahl
	 * dem Fenster hinzu
	 */
	public void createLanguageSelect() {

		activeFrame = 0;
		mainFrame.getContentPane().removeAll();
		label = new JLabel("Willkommen! Bitte w�hlen Sie ihre Sprache aus:");
		label2 = new JLabel("Welcome! Please select your Language:");
		background = new JPanel(gridBag);
		top = new JPanel(gridBag);
		top.setName("stripe");
		top.setPreferredSize(new Dimension(800, 100));
		middle = new JPanel(gridBag);
		body = new JPanel(gridBag);

		String path = FileManager.userDirectory + "/LookAndFeel/";
		try {
			BufferedImage englisch = ImageIO.read(new File(path + "GBR.png"));
			BufferedImage deutsch = ImageIO.read(new File(path + "GER.png"));
			label3 = new JLabel(new ImageIcon(englisch.getScaledInstance(300, 200, Image.SCALE_SMOOTH)));
			label4 = new JLabel(new ImageIcon(deutsch.getScaledInstance(300, 200, Image.SCALE_SMOOTH)));
		} catch (IOException e) {
			e.printStackTrace();
		}

		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(0, 0, 20, 0);
		c.gridy = 0;
		c.gridx = 0;
		middle.add(label, c);
		c.gridy++;
		middle.add(label2, c);

		c.insets = new Insets(40, 40, 40, 40);
		c.gridy = 0;
		c.weightx = 1;
		body.add(label3, c);
		c.gridx++;
		body.add(label4, c);

		c.insets = new Insets(0, 0, 0, 0);
		background.add(middle, c);
		c.gridy++;
		background.add(body, c);

		mainFrame.add(background);
		mainFrame.repaint();
	}

	/**
	 * Diese Methode f�gt alle visuellen Komponenten der Login-Sicht dem Fenster
	 * hinzu
	 */
	public void createLogin() {

		activeFrame = 1;
		remove();
		label = new JLabel(Translation.getText("login1"));
		label2 = new JLabel(Translation.getText("login2"));
		label3 = new JLabel(Translation.getText("login3"));
		label4 = new JLabel(Translation.getText("login4"));
		language = new JToggleButton();
		language.setPreferredSize(new Dimension(75, 50));
		language.setName("language");

		inputfield1 = new JTextField(10);
		inputfield1.requestFocus();
		password = new JPasswordField(10);
		password.setEditable(false);
		password.setFocusable(false);

		GridBagConstraints c = new GridBagConstraints();
		c.weightx = 1;
		c.weighty = 1;
		c.gridy = 0;
		c.gridx = 1;
		c.insets = new Insets(0, 0, 0, 100);
		c.anchor = GridBagConstraints.EAST;
		top.add(language, c);
		c.gridx = 0;
		c.insets = new Insets(0, 100, 30, 0);
		c.anchor = GridBagConstraints.WEST;
		middle.add(label2, c);
		c.gridy++;
		body.add(label3, c);
		c.gridy++;
		body.add(label4, c);
		c.gridy = 1;
		c.gridx++;
		c.insets = new Insets(0, 0, 30, 100);
		c.anchor = GridBagConstraints.EAST;
		body.add(inputfield1, c);
		c.gridy++;
		body.add(password, c);
		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(0, 0, 0, 0);
		c.fill = GridBagConstraints.HORIZONTAL;
		top.add(label, c);
		c.anchor = GridBagConstraints.NORTH;
		c.insets = new Insets(35, 0, 0, 0);
		background.add(top, c);
		c.insets = new Insets(0, 0, 0, 0);
		c.gridy++;
		background.add(middle, c);
		c.gridy++;
		c.insets = new Insets(0, 0, 100, 0);
		background.add(body, c);
		mainFrame.add(background);
		mainFrame.repaint();
	}

	/**
	 * Diese Methode f�gt alle visuellen Komponenten der Nutzer�bersicht dem Fenster
	 * hinzu
	 */
	public void createMenu() {

		activeFrame = 2;
		remove();
		label = new JLabel(Translation.getText("menu1"));
		b1 = new JButton(Translation.getText("menu2"));
		b1.setName("1");
		b2 = new JButton(Translation.getText("menu3"));
		b2.setName("2");
		b3 = new JButton(Translation.getText("menu4"));
		b3.setName("3");
		b4 = new JButton(Translation.getText("menu5"));
		b4.setName("4");
		exit = new JButton(Translation.getText("menu6"));

		GridBagConstraints c = new GridBagConstraints();
		c.weightx = 1;
		c.weighty = 1;
		c.gridy = 0;
		c.gridx = 0;
		c.fill = GridBagConstraints.HORIZONTAL;
		top.add(label, c);
		c.insets = new Insets(15, 0, 15, 0);
		c.gridy++;
		body.add(b1, c);
		c.gridy++;
		body.add(b2, c);
		c.gridy++;
		body.add(b3, c);
		c.gridy++;
		body.add(b4, c);
		c.gridy++;
		body.add(exit, c);
		c.gridy = 0;
		c.anchor = GridBagConstraints.NORTH;
		c.insets = new Insets(35, 0, 0, 0);
		background.add(top, c);
		c.gridy++;
		c.insets = new Insets(0, 100, 0, 100);
		background.add(body, c);
		mainFrame.add(background);
		mainFrame.repaint();
	}

	/**
	 * Diese Methode f�gt alle visuellen Komponenten der Konto�bersicht dem Fenster
	 * hinzu
	 */
	public void createBalanceGUI() {

		activeFrame = 3;
		remove();
		label = new JLabel(Translation.getText("balance1"));
		label2 = new JLabel(Translation.getText("balance2"));
		exit = new JButton(Translation.getText("back"));
		outputfield1 = new JTextArea(55, 42);
		outputfield1.setEditable(false);

		JScrollPane textArea = new JScrollPane(outputfield1);
		textArea.setName("scrollpane");
		textArea.setPreferredSize(new Dimension(500, 290));

		GridBagConstraints c = new GridBagConstraints();
		c.weightx = 1;
		c.weighty = 1;
		c.gridy = 0;
		c.gridx = 0;
		c.fill = GridBagConstraints.HORIZONTAL;
		top.add(label, c);
		c.anchor = GridBagConstraints.WEST;
		middle.add(label2, c);
		body.add(textArea, c);
		c.gridy++;
		c.insets = new Insets(20, 100, 20, 100);
		body.add(exit, c);

		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.NORTH;
		c.insets = new Insets(35, 0, 0, 0);
		background.add(top, c);
		c.gridy++;
		c.insets = new Insets(20, 50, 0, 50);
		c.weighty = 0;
		background.add(middle, c);
		c.gridy++;
		c.weighty = 1;
		background.add(body, c);

		mainFrame.add(background);
		mainFrame.repaint();
	}

	/**
	 * Diese Methode f�gt alle visuellen Komponenten der Abhebe-�bersicht dem
	 * Fenster hinzu
	 */
	public void createWithdrawGUI() {

		activeFrame = 4;
		remove();
		label = new JLabel(Translation.getText("Withdraw"));
		b1 = new JButton("5�");
		b1.setName("5");
		b2 = new JButton("10�");
		b2.setName("10");
		b3 = new JButton("20�");
		b3.setName("20");
		b4 = new JButton("50�");
		b4.setName("50");
		b5 = new JButton("100�");
		b5.setName("100");
		b6 = new JButton("200�");
		b6.setName("200");
		b7 = new JButton("500�");
		b7.setName("500");
		label2 = new JLabel(Translation.getText("withdraw2"));
		exit = new JButton(Translation.getText("back"));

		GridBagConstraints c = new GridBagConstraints();
		c.weightx = 1;
		c.weighty = 1;
		c.gridy = 0;
		c.gridx = 0;
		c.fill = GridBagConstraints.HORIZONTAL;
		top.add(label, c);
		c.anchor = GridBagConstraints.WEST;
		c.insets = new Insets(10, 25, 10, 0);
		c.fill = GridBagConstraints.HORIZONTAL;
		middle.add(label2, c);
		c.insets = new Insets(20, 25, 20, 25);
		c.anchor = GridBagConstraints.NORTH;
		c.gridy++;
		c.gridx = 0;
		body.add(b1, c);
		c.gridy++;
		body.add(b2, c);
		c.gridy++;
		body.add(b3, c);
		c.gridy++;
		body.add(b4, c);
		c.gridy = 1;
		c.gridx++;
		body.add(b5, c);
		c.gridy++;
		body.add(b6, c);
		c.gridy++;
		body.add(b7, c);
		c.gridy++;
		body.add(exit, c);
		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(35, 0, 0, 0);
		background.add(top, c);
		c.gridy++;
		c.insets = new Insets(0, 25, 0, 25);
		c.weighty = 0;
		background.add(middle, c);
		c.gridy++;
		c.weighty = 1;
		background.add(body, c);

		mainFrame.add(background);
		mainFrame.repaint();
	}

	/**
	 * Diese Methode f�gt alle visuellen Komponenten der Einzahlungs�bersicht dem
	 * Fenster hinzu
	 */
	public void createDepositGUI() {

		activeFrame = 5;
		remove();
		label = new JLabel(Translation.getText("Deposit"));
		label2 = new JLabel(Translation.getText("deposit2"));
		label3 = new JLabel(" �");
		inputfield2 = new JTextField(12);
		inputfield2.setHorizontalAlignment(SwingConstants.RIGHT);
		inputfield2.addKeyListener(new Currency());

		enter = new JButton(Translation.getText("depositEnter"));
		exit = new JButton(Translation.getText("back"));
		JPanel input = new JPanel(gridBag);

		GridBagConstraints c = new GridBagConstraints();
		c.weightx = 1;
		c.weighty = 1;
		c.gridy = 0;
		c.gridx = 0;
		c.fill = GridBagConstraints.HORIZONTAL;
		top.add(label, c);
		middle.add(label2, c);
		c.fill = GridBagConstraints.NONE;
		c.weightx = 0;
		input.add(inputfield2, c);
		c.gridx++;
		c.anchor = GridBagConstraints.WEST;
		input.add(label3);
		c.weightx = 1;
		c.gridy++;
		c.gridx = 0;
		c.insets = new Insets(10, 25, 10, 25);
		c.fill = GridBagConstraints.HORIZONTAL;
		body.add(exit, c);
		c.gridx++;
		body.add(enter, c);
		c.gridx = 0;
		c.gridy = 0;

		c.anchor = GridBagConstraints.NORTH;
		c.insets = new Insets(35, 0, 20, 0);
		background.add(top, c);
		c.insets = new Insets(20, 50, 20, 50);
		c.gridy++;
		background.add(middle, c);
		c.gridy++;
		background.add(input, c);
		c.gridy++;
		c.insets = new Insets(0, 25, 80, 25);
		background.add(body, c);
		mainFrame.add(background);
		mainFrame.repaint();
	}

	/**
	 * Diese Methode f�gt alle visuellen Komponenten der �berweisungs�bersicht dem
	 * Fenster hinzu
	 */
	public void createTransferGUI() {

		activeFrame = 6;
		remove();
		label = new JLabel(Translation.getText("Transfer"));
		label2 = new JLabel(Translation.getText("transfer2"));
		label3 = new JLabel(Translation.getText("transfer3"));
		label4 = new JLabel(Translation.getText("transfer4"));
		label5 = new JLabel(" �");
		inputfield1 = new JTextField(12);
		inputfield1.setHorizontalAlignment(SwingConstants.RIGHT);
		inputfield2 = new JTextField(12);
		inputfield2.setHorizontalAlignment(SwingConstants.RIGHT);
		inputfield2.setEditable(false);
		inputfield2.setFocusable(false);
		inputfield2.addKeyListener(new Currency());
		enter = new JButton(Translation.getText("transferEnter"));
		exit = new JButton(Translation.getText("back"));
		JPanel input = new JPanel(gridBag);

		GridBagConstraints c = new GridBagConstraints();
		c.weightx = 1;
		c.weighty = 1;
		c.gridy = 0;
		c.gridx = 0;
		c.fill = GridBagConstraints.HORIZONTAL;
		top.add(label, c);
		middle.add(label2, c);
		c.insets = new Insets(15, 0, 15, 0);
		c.anchor = GridBagConstraints.WEST;
		input.add(label3, c);
		c.gridy++;
		input.add(label4, c);
		c.gridy = 0;
		c.gridx++;
		input.add(inputfield1, c);
		c.gridy++;
		input.add(inputfield2, c);
		c.gridx++;
		input.add(label5, c);
		c.weightx = 1;
		c.gridy = 0;
		c.gridx = 0;
		c.insets = new Insets(10, 25, 10, 25);
		c.anchor = GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.HORIZONTAL;
		body.add(exit, c);
		c.gridx++;
		body.add(enter, c);
		c.gridx = 0;
		c.anchor = GridBagConstraints.NORTH;
		c.insets = new Insets(35, 0, 0, 0);
		background.add(top, c);
		c.insets = new Insets(40, 50, 20, 50);
		c.gridy++;
		background.add(middle, c);
		c.gridy++;
		background.add(input, c);
		c.gridy++;
		c.insets = new Insets(20, 25, 80, 25);
		background.add(body, c);
		mainFrame.add(background);
		mainFrame.repaint();
	}

	/**
	 * Diese Methode f�gt alle visuellen Komponenten der Admin-Sicht dem Fenster
	 * hinzu
	 */
	public void createAdminPage() {

		activeFrame = 7;
		remove();
		Border border = new LineBorder(Color.black, 2);
		Border title2 = new TitledBorder(border, Translation.getText("adminAdd") + " ");

		inputfield1 = new JTextField(10);
		inputfield1.setText(String.valueOf(FileManager.getNewAccountNumber(false)));
		inputfield1.setEditable(false);
		inputfield1.setFocusable(false);
		inputfield2 = new JTextField(10);
		inputfield3 = new JTextField(10);
		inputfield4 = new JTextField(10);
		checkBox = new JCheckBox();

		label = new JLabel(Translation.getText("admin1"));
		label2 = new JLabel(Translation.getText("admin2"));
		label3 = new JLabel(Translation.getText("admin3"));
		label4 = new JLabel("PIN [5]: ");
		label5 = new JLabel(Translation.getText("admin4"));

		b1 = new JButton(Translation.getText("adminAdd"));
		b2 = new JButton(Translation.getText("adminDelete"));
		b3 = new JButton(Translation.getText("shutdown"));
		exit = new JButton(Translation.getText("logout"));

		JPanel pLeft = new JPanel(gridBag);
		JPanel pRight = new JPanel(gridBag);
		JScrollPane table = new JScrollPane(Table.table);
		table.setPreferredSize(new Dimension(450, 250));

		outputfield1 = new JTextArea(55, 42);
		outputfield1.setEditable(false);

		JScrollPane textArea = new JScrollPane(outputfield1);
		textArea.setName("scrollpane");
		textArea.setPreferredSize(new Dimension(450, 200));

		GridBagConstraints c = new GridBagConstraints();
		c.gridy = 0;
		c.gridx = 0;
		c.gridwidth = 2;
		pLeft.add(table, c);
		c.gridwidth = 1;
		c.gridy++;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1;
		c.insets = new Insets(20, 0, 0, 10);
		pLeft.add(exit, c);
		c.gridx++;
		c.insets = new Insets(20, 10, 0, 0);
		pLeft.add(b2, c);

		c.gridx = 0;
		c.gridy = 0;
		c.insets = new Insets(5, 5, 5, 5);
		pRight.add(label, c);
		c.gridx++;
		pRight.add(inputfield1, c);
		c.gridx = 0;
		c.gridy++;
		pRight.add(label2, c);
		c.gridx++;
		pRight.add(inputfield2, c);
		c.gridx = 0;
		c.gridy++;
		pRight.add(label3, c);
		c.gridx++;
		pRight.add(inputfield3, c);
		c.gridx = 0;
		c.gridy++;
		pRight.add(label4, c);
		c.gridx++;
		pRight.add(inputfield4, c);
		c.gridx = 0;
		c.gridy++;
		pRight.add(label5, c);
		c.gridx++;
		pRight.add(checkBox, c);
		c.gridx = 0;
		c.gridy++;
		c.gridwidth = 2;
		pRight.add(b1, c);
		pRight.setBorder(title2);

		c = new GridBagConstraints();
		c.gridy = 0;
		c.gridx = 0;
		c.insets = new Insets(0, 20, 0, 20);
		background.add(pLeft, c);
		c.gridy++;
		c.insets = new Insets(20, 20, 0, 20);
		background.add(textArea, c);
		c.gridy = 0;
		c.gridx++;
		background.add(pRight, c);
		c.gridy++;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.NORTH;
		background.add(b3, c);
		mainFrame.add(background);
		changeFont(background, new Font("DIALOG", Font.PLAIN, 14));
		mainFrame.repaint();
	}

	/**
	 * Diese Methode entfernt alle Komponenten der vorherigen Sicht von den
	 * Containern
	 */
	public void remove() {
		mainFrame.getContentPane().removeAll();
		background.removeAll();
		top.remove(label);
		middle.removeAll();
		body.removeAll();
	}

	/**
	 * Diese Methode �ndert die Schriftart einer Komponente und all ihren
	 * Unterkomponenten
	 * 
	 * @param component Komponente
	 * @param font      Schriftart
	 */
	public static void changeFont(Component component, Font font) {
		component.setFont(font);
		if (component instanceof Container) {
			for (Component child : ((Container) component).getComponents()) {
				changeFont(child, font);
			}
		}
	}

	/*
	 * Diese innere Klasse formatiert das Eingabefeld f�r Geldbetr�ge
	 */
	private class Currency implements KeyListener {

		@Override
		public void keyTyped(KeyEvent e) {
			if (Character.toString(e.getKeyChar()).matches("\\d")) {
				String text = inputfield2.getText();
				String[] arr = new String[2];
				String output = "";
				if (text.contains(",")) {
					arr = text.split(",");
					String after = arr[1].substring(1);
					String before = arr[1].substring(0, 1);
					output = arr[0] + before + "," + after;
					inputfield2.setText(output);
				} else {
					if (text.length() > 1) {
						text = text.substring(0, 1) + "," + text.substring(1);
						inputfield2.setText(text);
					}
				}
			}
		}

		@Override
		public void keyPressed(KeyEvent e) {

		}

		@Override
		public void keyReleased(KeyEvent e) {
		}
	}

	/**
	 * Diese Methode l�dt die XML-Konfigurationsdatei f�r die visuelle
	 * Erscheinungsbild aller Komponenten
	 */
	public void setLookAndFeel() {

		SynthLookAndFeel laf = new SynthLookAndFeel();
		try {
			laf.load(new URL("file:///" + FileManager.userDirectory + "/LookAndFeel/laf.xml"));
			UIManager.setLookAndFeel(laf);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}