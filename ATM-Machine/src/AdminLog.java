import java.util.Collections;
import java.util.List;

/**
 * Diese Klasse dient dazu, das Json-Objekt aus der Admin-Logdatei in ein
 * Javaobjekt umzuwandeln
 * 
 * @author Elijah Seyfarth
 *
 */
public class AdminLog {

	private LogTypes logType;
	private String logCreationTime;
	private String accountNumber;

	/**
	 * Diese Methode formatiert Zeile f�r Zeile die Logeintr�ge der Admin-Sicht und
	 * gibt diese als Gesamttext aus
	 * 
	 * @return formatierter Log
	 */
	public static String getAdminLog() {

		List<AdminLog> adminLogs = FileManager.getAdminLog();
		Collections.reverse(adminLogs);

		String type = "";
		String accNr = "";
		String date = "";

		String divide = " ";
		for (int i = 0; i < 113; i++) {
			divide += "-";
		}
		divide += "\n";

		String text = divide;
		for (AdminLog log : adminLogs) {

			type = log.getLogType().toString();
			accNr = log.getAccountNumber();
			date = log.getLogCreationTime();

			text += " " + date + " - " + type + ": " + accNr + "\n";
			text += divide;
		}
		return text;
	}

	/**
	 * 
	 * @return Aktionstyp
	 */
	public LogTypes getLogType() {
		return logType;
	}

	/**
	 * 
	 * @param logType Aktionstyp
	 */
	public void setLogType(LogTypes logType) {
		this.logType = logType;
	}

	/**
	 * 
	 * @return Zeitstempel
	 */
	public String getLogCreationTime() {
		return logCreationTime;
	}

	/**
	 * 
	 * @param logCreationTime Zeitstempel
	 */
	public void setLogCreationTime(String logCreationTime) {
		this.logCreationTime = logCreationTime;
	}

	/**
	 * 
	 * @return Kontonummer des Ziels
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * 
	 * @param accountNumber Kontonummmer des Ziels
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

}
