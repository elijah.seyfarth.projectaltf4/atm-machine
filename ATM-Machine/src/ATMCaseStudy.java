/**
 * 
 * Klasse dient dem Start des Bankautomaten und erstellt dabei eine 
 * einmalige Instanz um Daten konsistent zu halten.
 * 
 * @author Marco
 *
 */

public class ATMCaseStudy {
	
	public static void main(String[] args) {

		ATM theATM = ATM.getinstance();

		theATM.run();
	}
}