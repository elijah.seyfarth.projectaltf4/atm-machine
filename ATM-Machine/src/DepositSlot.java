/**
 * 
 * Klasse dient der Eingabebestaetigung von Geldumschlaegen.
 * 
 * @author Marco
 *
 */

public class DepositSlot {
	
	/**
	 * Methode bestaetigt die Uebername eines Geldumschlags.
	 * 
	 * @return Bestaetigung
	 */
	public boolean isEnvelopeReceived() {
		return true;
	}

}
