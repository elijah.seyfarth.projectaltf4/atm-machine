import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Diese Klasse fuehrt die Einzahlung aus
 * 
 * @author Simon
 *
 */
public class Deposit extends Transaction {

	private DepositSlot depositSlot;
	private final static int CANCELED = 0;

	/**
	 * Der Konstuktor ruft den Superkonstruktor mit den eingegebenen Werten auf und
	 * legt die Eingabebestaetigung fest
	 * 
	 * @param userAccountNumber Kontonummer
	 * @param atmScreen         ATM-Instanz
	 * @param atmBankDatabase   Datenbankinstanz
	 * @param atmDepositSlot    DepositSlot-Instanz
	 * @param atm               ATM-Instanz
	 */
	public Deposit(String userAccountNumber, Screen atmScreen, BankDatabase atmBankDatabase, DepositSlot atmDepositSlot,
			ATM atm) {

		super(userAccountNumber, atmScreen, atmBankDatabase, atm);

		depositSlot = atmDepositSlot;
	}

	/**
	 * Diese Methode startet die Einzahlung
	 */
	@Override
	public void execute() {
		promptForDepositAmount();
	}

	/**
	 * Diese Methode erstellt die Texte fuer die Anzeige
	 * 
	 * @param amount Die eingezahlte Menge
	 */
	public void makedeposit(double amount) {

		BankDatabase bankDatabase = getBankDatabase();

		if (amount != CANCELED) {

			screen.label2.setText(Translation.getText("depositMes1") + amount);
			boolean envelopeReceived = depositSlot.isEnvelopeReceived();

			if (envelopeReceived) {
				screen.label2.setText(Translation.getText("depositConf"));
				bankDatabase.credit(getAccountNumber(), amount);
				FileManager.logTransaction(getAccountNumber(), getAccountNumber(), LogTypes.DEPOSIT, amount);
			} else {
				screen.label2.setText(Translation.getText("depositErr1"));
			}
		} else {
			screen.label2.setText(Translation.getText("cancel"));
		}
	}

	/**
	 * Diese Methode erstellt die Ansicht fuer die Einzahlung
	 */
	private void promptForDepositAmount() {

		screen.createDepositGUI();
		atm.switchStatus(false, false);
		Depositcheck check = new Depositcheck();
		screen.enter.addActionListener(check);
		screen.mainFrame.revalidate();
	}

	/**
	 * Diese innere Klasse stellt die ActionListener zur Verfuegung
	 * 
	 * @author Simon
	 *
	 */
	private class Depositcheck implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {

			try {
				double amount = Double.parseDouble(screen.inputfield2.getText().replace(",", "."));
				makedeposit(amount);
			} catch (NumberFormatException nfe) {
				screen.label2.setText(Translation.getText("inputErr1"));
				screen.inputfield2.requestFocus();
			}
			screen.inputfield2.setText("");
		}
	}
}