import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Diese Klasse fuehrt Ueberweisungen aus
 * 
 * @author Simon
 *
 */
public class Transfer extends Transaction {

	/**
	 * Der Konstruktor ruft den Superkonstruktor mit den eingegebenen Werten auf
	 * 
	 * @param userAccountNumber Kontonummer
	 * @param atmScreen         Fensterinstanz
	 * @param atmBankDatabase   Datenbankinstanz
	 * @param atm               ATM-Instanz
	 */
	public Transfer(String userAccountNumber, Screen atmScreen, BankDatabase atmBankDatabase, ATM atm) {

		super(userAccountNumber, atmScreen, atmBankDatabase, atm);
	}

	/**
	 * Diese Methode startet die Ueberweisung
	 */
	@Override
	public void execute() {
		promptForTransfer();
	}

	/**
	 * Diese Methode fuehrt die Ueberweisung aus
	 * 
	 * @param toAccountNumber Kontonummer des Zielkontos
	 * @param amount          Der zu ueberweisende Betrag
	 */
	public void makeTransfer(String toAccountNumber, double amount) {

		BankDatabase bankDatabase = getBankDatabase();
		Screen screen = getScreen();

		bankDatabase.credit(toAccountNumber, amount);
		bankDatabase.debit(getAccountNumber(), amount);
		FileManager.logTransaction(getAccountNumber(), toAccountNumber, LogTypes.TRANSFER, -amount);
		FileManager.logTransaction(toAccountNumber, getAccountNumber(), LogTypes.TRANSFER, amount);
		screen.label2.setText(Translation.getText("transferConf"));
		atm.switchStatus(true, false);
		screen.mainFrame.revalidate();
	}

	/**
	 * Diese Methode erstellt die Texte fuer die Anzeige
	 */
	private void promptForTransfer() {

		screen.createTransferGUI();
		Transfercheck check = new Transfercheck();
		Enter check1 = new Enter();
		atm.switchStatus(true, false);
		screen.inputfield1.addKeyListener(check1);
		screen.inputfield2.addKeyListener(check1);
		screen.enter.addActionListener(check);
		screen.mainFrame.revalidate();
	}

	/**
	 * Diese innere Klasse stellt die KeyListener zur Verfuegung
	 * 
	 * @author Simon
	 *
	 */
	public class Enter implements KeyListener {

		@Override
		public void keyTyped(KeyEvent e) {
		}

		@Override
		public void keyPressed(KeyEvent e) {

			atm.lastAction();
			if (e.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE) {

				if (atm.status == 1) {
					if (screen.inputfield2.getText().length() == 0) {
						atm.switchStatus(false, false);
					}
					screen.inputfield2.setText("");
				}
				if (atm.status == 0) {
					screen.inputfield1.setText("");
				}
			}
			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				if (atm.status == 0) {
					atm.switchStatus(false, false);
				}
			}
		}

		@Override
		public void keyReleased(KeyEvent e) {
		}
	}

	/**
	 * Diese innere Klasse stellt die ActionListener zur Verfuegung
	 * 
	 * @author Simon
	 *
	 */
	private class Transfercheck implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {

			try {
				String accNr = screen.inputfield1.getText();
				Integer.parseInt(accNr);
				double amount = Double.parseDouble(screen.inputfield2.getText().replace(",", "."));
				makeTransfer(accNr, amount);
			} catch (NumberFormatException e2) {
				screen.label2.setText(Translation.getText("inputErr1"));
				screen.inputfield2.setText("");
				screen.inputfield2.requestFocus();
			} catch (Exception e1) {
				screen.label2.setText(Translation.getText("transferErr1"));
				atm.switchStatus(true, false);
			}
		}
	}
}