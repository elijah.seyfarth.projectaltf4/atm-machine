import java.util.HashMap;

/**
 * 
 * Klasse dient der zweisprachigen Darstellbarkeit der Benutzeroberflaeche
 * des Bankautomaten in Deutsch und Englisch.
 * 
 * @author Marco
 *
 */

public class Translation {

	/**
	 * Es werden 2 HashMaps erstellt die �ber einen Schl�sselwert den Zugriff auf
	 * einzelne Textnachrichten erm�glichen.
	 */

	static HashMap<String, String> deutsch = new HashMap<String, String>();
	static HashMap<String, String> englisch = new HashMap<String, String>();

	/**
	 * Methode dient der Zuordnung der Textnachricht ueber die gewaehlten Sprache und den
	 * Schluesselwert.
	 * 
	 * @param key Schluesselwert
	 * @return Textausgabe in Deutsch falls ausgewaehlt, sonst Englisch
	 */
	public static String getText(String key) {
		if (ATM.language.equals("ger")) {
			return deutsch.get(key);
		}
		return englisch.get(key);
	}
	/**
	 * Methode dient der Zuordnung der Textnachricht ueber die gewaehlten Sprache und den
	 * Schluesselwert.
	 * 
	 * @param key Schluesselwert
	 * @return Textausgabe in Englisch falls ausgewaehlt, sonst Deutsch
	 */
	public static String getTextReversed(String key) {
		if (ATM.language.equals("ger")) {
			return englisch.get(key);
		}
		return deutsch.get(key);
	}
	
	/**
	 * 
	 * Methode gibt uebersetzte Spaltennamen in 
	 * Form eines String-Arrays zurueck
	 * 
	 * @return String-Array
	 */
	
	public static String[] columnNames() {

		String[] arr = new String[5];

		arr[0] = getText("table1");
		arr[1] = "PIN";
		arr[2] = getText("table2");
		arr[3] = getText("table3");
		arr[4] = getText("table4");

		return arr;
	}
	
	/**
	 * Methode dient der Uebersetzung der Textausgabe der Log-Datei.
	 * 
	 * @param type Enum-Konstante
	 * @return Textausgabe 
	 */
	public static String translateType(LogTypes type) {

		return getText(type.toString());
	}

	/**
	 * Methode macht darstellbare Textnachrichten erreichbar ueber
	 * zugeordnete Schl�sselwerte.
	 */
	public static void fill() {

		englisch.put("login1", "Login"); // Login
		deutsch.put("login1", "Anmeldung");

		englisch.put("login2", "Insert your credit/debit card first.");
		deutsch.put("login2", "Bitte zuerst Kredit-/Debitkarte einf�hren.");

		englisch.put("login3", "Enter your account number: ");
		deutsch.put("login3", "Kontonummer eingeben: ");

		englisch.put("login4", "Enter your PIN number: ");
		deutsch.put("login4", "PIN-Nummer eingeben: ");

		englisch.put("loginErr1", "Invalid account number. Please try again.");
		deutsch.put("loginErr1", "Ung�ltige Kontonummer. Bitte nochmal versuchen.");

		englisch.put("loginErr2", "Invalid PIN. Please try again.");
		deutsch.put("loginErr2", "Ung�ltige PIN. Bitte nochmal versuchen.");

		englisch.put("loginErr3", "Wrong PIN entered 3 times. The card will remain in the Machine.");
		deutsch.put("loginErr3", "Die PIN wurde 3 mal falsch eingegeben. Die Karte wird einbehalten.");

		englisch.put("menu1", "Welcome "); // User Menu
		deutsch.put("menu1", "Willkommen ");

		englisch.put("menu2", "Balance");
		deutsch.put("menu2", "Kontostand");

		englisch.put("menu3", "Withdrawal");
		deutsch.put("menu3", "Abhebung");

		englisch.put("menu4", "Deposit");
		deutsch.put("menu4", "Einzahlung");

		englisch.put("menu5", "Transfer");
		deutsch.put("menu5", "�berweisung");

		englisch.put("menu6", "Exit");
		deutsch.put("menu6", "Beenden");

		englisch.put("balance1", "Balance Information"); // Balance GUI
		deutsch.put("balance1", "Konto�bersicht");

		englisch.put("balance2", "Balance: ");
		deutsch.put("balance2", "Kontostand: ");

		englisch.put("balanceLog", "Transaction history: ");
		deutsch.put("balanceLog", "Transaktionsverlauf: ");

		englisch.put("from", "From: ");
		deutsch.put("from", "Von: ");

		englisch.put("to", "To: ");
		deutsch.put("to", "Nach: ");

		englisch.put("Withdraw", "Withdraw Menu"); // Withdraw GUI
		deutsch.put("Withdraw", "Geldabhebung");

		englisch.put("withdraw2", "Please choose an amount to withdraw:");
		deutsch.put("withdraw2", "Bitte w�hlen Sie einen Betrag zum Abheben:");

		englisch.put("withdrawConf", "Your cash has been dispensed. Please take your cash now.");
		deutsch.put("withdrawConf", "Ihr Bargeld wurde ausgezahlt. Bitte entnehmen Sie es jetzt.");

		englisch.put("insufficient2", "Insufficient cash available in the ATM.");
		deutsch.put("insufficient2", "Nicht gen�gend Bargeld im Geldautomat vorhanden.");

		englisch.put("Deposit", "Deposit Menu"); // Deposit GUI
		deutsch.put("Deposit", "Geldeinzahlung");

		englisch.put("deposit2", "Please enter the deposit amount:");
		deutsch.put("deposit2", "Bitte geben Sie den Einzahlungsbetrag ein:");

		englisch.put("depositEnter", "Deposit");
		deutsch.put("depositEnter", "Einzahlen");

		englisch.put("depositMes1", "Please insert a deposit envelope containing ");
		deutsch.put("depositMes1", "Bitte legen Sie einen Umschlag mit folgendem Betrag ein ");

		englisch.put("depositConf", "Your envelope has been received and will be checked later.");
		deutsch.put("depositConf", "Ihr Umschlag wurde erhalten und wird demn�chst gepr�ft");

		englisch.put("depositErr1", "You did not insert an envelope, so the transaction was canceled.");
		deutsch.put("depositErr1", "Es wurde kein Umschlag erkannt, die Transaktion wurde abgebrochen.");

		englisch.put("Transfer", "Transfer"); // Transfer GUI
		deutsch.put("Transfer", "�berweisung");

		englisch.put("transfer2", "Please enter the transfer data:");
		deutsch.put("transfer2", "Bitte geben Sie die �berweisungsdaten ein:");

		englisch.put("transfer3", "Receiving account number: ");
		deutsch.put("transfer3", "Kontonummer des Empf�ngers: ");

		englisch.put("transfer4", "Enter a transfer amount: ");
		deutsch.put("transfer4", "Zu �berweisender Betrag: ");

		englisch.put("transferEnter", "Transfer");
		deutsch.put("transferEnter", "�berweisen");

		englisch.put("transferConf", "The cash has been transferred.");
		deutsch.put("transferConf", "Der Geldbetrag wurde �berwiesen.");

		englisch.put("transferErr1", "The entered account number does not exist.");
		deutsch.put("transferErr1", "Die eingegebene Kontonummer gibt es nicht.");

		englisch.put("admin1", "Account number: "); // Admin GUI
		deutsch.put("admin1", "Kontonummer: ");

		englisch.put("admin2", "Username: ");
		deutsch.put("admin2", "Benutzername: ");

		englisch.put("admin3", "Balance: ");
		deutsch.put("admin3", "Kontostand: ");

		englisch.put("admin4", "Admin: ");
		deutsch.put("admin4", "Administrator: ");

		englisch.put("adminDelete", "Delete");
		deutsch.put("adminDelete", "L�schen");

		englisch.put("adminAdd", "Add Account");
		deutsch.put("adminAdd", "Konto hinzuf�gen");

		englisch.put("shutdown", "Shutdown ATM");
		deutsch.put("shutdown", "Geldautomat abschalten");

		englisch.put("adminMes1", " Delete account: '");
		deutsch.put("adminMes1", " Nutzer l�schen: '");

		englisch.put("adminErr1", "Invalid Input");
		deutsch.put("adminErr1", "Ung�ltige Eingabe");

		englisch.put("table1", "Account Number"); // Table Colums (Admin GUI)
		deutsch.put("table1", "Kontonr.");

		englisch.put("table2", "Username");
		deutsch.put("table2", "Nutzername");

		englisch.put("table3", "Balance");
		deutsch.put("table3", "Kontostand");

		englisch.put("table4", "isAdmin");
		deutsch.put("table4", "Admin");

		englisch.put("back", "Back"); // Allgemeines
		deutsch.put("back", "Zur�ck");

		englisch.put("cancel", "Canceling transaction...");
		deutsch.put("cancel", "Transaktion wird abgebrochen...");

		englisch.put("logout", "Logout");
		deutsch.put("logout", "Abmelden");

		englisch.put("clear", "Clear");
		deutsch.put("clear", "L�schen");

		englisch.put("enter", "Enter");
		deutsch.put("enter", "Best�tigen");

		englisch.put("language", "Change language");
		deutsch.put("language", "Sprache �ndern");

		englisch.put("inputErr1", "Please enter a valid amount.");
		deutsch.put("inputErr1", "Bitte geben Sie einen g�ltigen Betrag ein.");
	}
}