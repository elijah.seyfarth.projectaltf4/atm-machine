import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * Diese Klasse ist die Hauptklasse der ATM und repr�sentiert die funktionale
 * Ebene des Geldautomaten
 * 
 * @author Elijah Seyfarth
 *
 */
public class ATM {

	private String currentAccountNumber;
	private Screen screen;
	private CashDispenser cashDispenser;
	private DepositSlot depositSlot;
	private BankDatabase bankDatabase;
	private static ATM uniqueinstance;
	public static String language;
	private int userAuthenticated;
	private int failed;
	public int status;
	boolean admin = false;
	Timer t;

	/**
	 * Der Konstruktor erzeugt die notwendigen Objekte
	 */
	public ATM() {

		screen = new Screen();
		cashDispenser = new CashDispenser();
		depositSlot = new DepositSlot();
		bankDatabase = new BankDatabase();
	}

	/**
	 * Der Startpunkt der Klasse
	 */
	public void run() {
		createFrame();
	}

	/**
	 * Diese Methode erzeugt das Fenster
	 */
	void createFrame() {

		screen.initialize();
		screen.mainFrame.setUndecorated(true);
		screen.mainFrame.setSize(800, 600);
		screen.mainFrame.setLocationRelativeTo(null);
		screen.mainFrame.setVisible(true);
		screen.mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		screen.mainFrame.revalidate();
		languageSelection();
	}

	/*
	 * Diese Methode erzeugt die anf�ngliche Sprachauswahl
	 */
	void languageSelection() {

		t = new Timer();
		screen.createLanguageSelect();
		LanguageSelect check = new LanguageSelect();
		screen.mainFrame.addMouseListener(check);
		screen.mainFrame.revalidate();
	}

	/*
	 * Diese innere Klasse bearbeitet die Benutzerinteraktion mit der anf�nglichen
	 * Sprachauswahl
	 */
	private class LanguageSelect implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {

			if (e.getX() < screen.mainFrame.getWidth() / 2) {
				language = "gbr";
			} else {
				language = "ger";
			}
			startLogin();
			screen.mainFrame.removeMouseListener(this);
			lastAction();
		}

		@Override
		public void mousePressed(MouseEvent e) {
		}

		@Override
		public void mouseReleased(MouseEvent e) {
		}

		@Override
		public void mouseEntered(MouseEvent e) {
		}

		@Override
		public void mouseExited(MouseEvent e) {
		}
	}

	/*
	 * Diese innere KLasse bearbeitet den Knopfdruck der integrierten Sprachauswahl
	 */
	private class LanguageCheck implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {

			if (language.equals("ger")) {
				language = "gbr";
			} else {
				language = "ger";
			}
			lastAction();
			repaintOpenMenu();
		}
	}

	/**
	 * Diese Methode bereitet den Login vor
	 */
	void startLogin() {

		userAuthenticated = 0;
		failed = 0;
		currentAccountNumber = "";
		screen.createLogin();

		Authenticate check = new Authenticate();
		LanguageCheck language = new LanguageCheck();
		switchStatus(true, true);
		screen.inputfield1.addKeyListener(check);
		screen.password.addKeyListener(check);
		screen.language.addActionListener(language);
		screen.mainFrame.revalidate();
	}

	/*
	 * Diese innere Klasse verarbeitet die Benutzeraktionen innerhalb des Logins
	 */
	public class Authenticate implements KeyListener {

		@Override
		public void keyTyped(KeyEvent e) {
		}

		@Override
		public void keyPressed(KeyEvent e) {

			lastAction();
			if (e.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE) {

				if (status == 1) {
					String pin = new String(screen.password.getPassword());
					if (pin.length() == 0) {
						switchStatus(false, true);
					}
					screen.password.setText("");
				}
				if (status == 0) {
					screen.inputfield1.setText("");
				}
			}

			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				if (status == 0) {
					switchStatus(false, true);
				} else if (status == 1) {

					String accNr = screen.inputfield1.getText();
					String pin = new String(screen.password.getPassword());
					authenticateUser(accNr, pin);
				}
			}
		}

		@Override
		public void keyReleased(KeyEvent e) {
		}
	}

	/**
	 * Diese Methode ist f�r die Anmeldung des Nutzers zust�ndig
	 * 
	 * @param accNr Kontonummer
	 * @param pin   PIN
	 */
	public void authenticateUser(String accNr, String pin) {
		userAuthenticated = bankDatabase.authenticateUser(accNr, pin);

		if (userAuthenticated > 1) {
			if (!bankDatabase.isAdmin(accNr)) {
				currentAccountNumber = accNr;
				screen.mainFrame.getContentPane().removeAll();
				bankDatabase.loadUser(accNr);
				createMenu();
				switchStatus(true, true);
				screen.mainFrame.revalidate();
			} else {
				screen.clearFields();
				createAdminGUI();
				screen.mainFrame.setLocationRelativeTo(null);
				admin = true;
				lastAction();
			}
		} else {
			if (userAuthenticated == 0) {
				screen.label2.setText(Translation.getText("loginErr1"));
			} else if (userAuthenticated == 1) {
				failed++;
				if (failed > 2) {
					screen.label2.setText(Translation.getText("loginErr3"));
				} else {
					screen.label2.setText(Translation.getText("loginErr2"));
				}
			}
			switchStatus(true, true);
		}
	}

	/**
	 * Diese Methode erstellt die Nutzer�bersicht
	 */
	public void createMenu() {

		screen.mainFrame.getContentPane().removeAll();
		MenuCheck check = new MenuCheck();
		screen.createMenu();
		screen.addActionListeners(check);
		Account Account1 = BankDatabase.getAccount(currentAccountNumber);
		screen.label.setText(Translation.getText("menu1") + Account1.getUsername());
		screen.mainFrame.revalidate();
	}

	/*
	 * Diese innere Klasse verarbeitet die Auswahl des Benutzers im Men�
	 */
	private class MenuCheck implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			lastAction();
			if (e.getSource() == screen.exit) {
				startLogin();
			} else {
				JButton b = (JButton) e.getSource();
				performTransactions(Integer.parseInt(b.getName()));
				switchStatus(true, true);
			}
		}
	}

	/**
	 * Diese Methode f�hrt den gew�hlten Men�punkt aus
	 * 
	 * @param a Die auszuf�hrende Transaktion
	 */
	private void performTransactions(int a) {

		Transaction currentTransaction = createTransaction(a);
		screen.clearFields();
		currentTransaction.execute();

		Backcheck Back = new Backcheck();
		screen.exit.addActionListener(Back);
		screen.mainFrame.revalidate();
	}

	/*
	 * Diese innere Klasse verwaltet den Zur�ck-Knopf in allen Benutzersichten
	 */
	public class Backcheck implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			createMenu();
			lastAction();
		}
	}

	/**
	 * Diese Methode erstellt die gew�hlte Nutzer-Sicht
	 * 
	 * @param type Transaktionstyp
	 * @return Die auszuf�hrende Transaktion
	 */
	private Transaction createTransaction(int type) {

		Transaction temp = null;

		if (type == 1)
			temp = new BalanceInquiry(currentAccountNumber, screen, bankDatabase, this);
		else if (type == 2)
			temp = new Withdrawal(currentAccountNumber, screen, bankDatabase, cashDispenser, this);
		else if (type == 3) {
			temp = new Deposit(currentAccountNumber, screen, bankDatabase, depositSlot, this);
		} else if (type == 4) {
			temp = new Transfer(currentAccountNumber, screen, bankDatabase, this);
		}
		return temp;
	}

	/**
	 * Diese Methode erstellt die Admin-Sicht
	 */
	public void createAdminGUI() {

		screen.mainFrame.getContentPane().removeAll();
		AdminCheck check = new AdminCheck();
		bankDatabase.loadAllAccounts();
		new Table();
		screen.createAdminPage();
		screen.showAdminLog();
		screen.checkBox.addActionListener(check);
		screen.addActionListeners(check);
		screen.inputfield1.setText(String.valueOf(FileManager.getNewAccountNumber(false)));
		screen.inputfield1.setVisible(true);
		screen.mainFrame.revalidate();
	}

	/*
	 * Diese Klasse verwaltet die Schaltfl�chen der Admin-Sicht
	 */
	private class AdminCheck implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == screen.b1) {
				try {
					String name = screen.inputfield2.getText();
					int balance = Integer.parseInt(screen.inputfield3.getText());
					String pin = screen.inputfield4.getText();
					boolean admin = screen.checkBox.isSelected();
					bankDatabase.addAccount(name, balance, pin, admin);
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, Translation.getText("adminErr1"));
				}

				screen.clearFields();
				screen.inputfield1.setText(FileManager.getNewAccountNumber(false));
				screen.showAdminLog();
			}
			if (e.getSource() == screen.b2) {
				String message = Translation.getText("adminMes1") + bankDatabase.getNrFrom(Table.table.getSelectedRow())
						+ "'?";
				int delete = JOptionPane.showConfirmDialog(null, message, null, JOptionPane.YES_NO_OPTION);
				if (delete == 0) {
					bankDatabase.deleteAccount(Table.table.getSelectedRow());
					screen.showAdminLog();
					screen.mainFrame.revalidate();
				}
			}
			if (e.getSource() == screen.b3) {
				System.exit(0);
			}
			if (e.getSource() == screen.checkBox) {
				screen.inputfield1.setText(FileManager.getNewAccountNumber(screen.checkBox.isSelected()));
			}
			if (e.getSource() == screen.exit) {
				startLogin();
				screen.clearFields();
				admin = false;
			}
			lastAction();
			screen.mainFrame.revalidate();
		}
	}

	/**
	 * Diese Methode f�hrt einen Fokuswechsel der Eingabefelder durch
	 * 
	 * @param reset Ob ein Reset der Eingabefelder durchgef�hrt werden soll
	 * @param pw    Ob ein Eingabefeld ein Passwortfeld ist
	 */
	public void switchStatus(boolean reset, boolean pw) {

		if (reset) {
			status = 0;
			screen.clearFields();
		} else {
			status = 1 - status;
		}
		if (status == 0) {
			if (pw) {
				screen.switchComponent(screen.inputfield1, screen.password);
			} else {
				screen.switchComponent(screen.inputfield1, screen.inputfield2);
			}
		} else {
			if (pw) {
				screen.switchComponent(screen.password, screen.inputfield1);
			} else {
				screen.switchComponent(screen.inputfield2, screen.inputfield1);
			}
		}
	}

	/**
	 * Diese Methode aktualisiert die jeweils aktualle Sicht
	 */
	public void repaintOpenMenu() {

		switch (screen.activeFrame) {
		case 1:
			startLogin();
			break;
		case 2:
			createMenu();
			break;
		case 3:
			performTransactions(1);
			break;
		case 4:
			performTransactions(2);
			break;
		case 5:
			performTransactions(3);
			break;
		case 6:
			performTransactions(4);
			break;
		default:

		}
	}

	/**
	 * Diese Methode setzt den Timer f�r die automatische Abmeldung zur�ck
	 */
	public void lastAction() {

		t.cancel();
		if (!admin) {
			t = new Timer();
			t.schedule(new Idle(), 60000); // 1 minute
		}
	}

	/*
	 * Diese innere Klasse ruft die anf�ngliche Sprachauswahl auf, wenn der Timer
	 * abgelaufen ist
	 */
	private class Idle extends TimerTask {

		@Override
		public void run() {
			languageSelection();
		}
	}

	/**
	 * Diese Methode ist Teil des Singleton-Prinzips und verhindert die Erzeugung
	 * einer zweiten Instanz der ATM
	 */
	public static ATM getinstance() {

		if (uniqueinstance == null) {
			uniqueinstance = new ATM();
		}
		return uniqueinstance;
	}
}
