import java.util.Collections;
import java.util.List;

/**
 * Diese Klasse dient dazu, das Json-Objekt aus der Nutzer-Logdatei in ein
 * Javaobjekt umzuwandeln
 * 
 * @author Elijah Seyfarth
 *
 */
public class UserLog {

	private double totalBalance;
	private double amount;
	private LogTypes logType;
	private String logCreationTime;
	private String otherAccount;

	/**
	 * Diese Methode formatiert Zeile f�r Zeile die Logeintr�ge eines Nutzers und
	 * gibt diese als Gesamttext aus
	 * 
	 * @param accNr Kontonummer
	 * @return formatierter Log
	 */
	public static String getCustomerLogs(String accNr) {

		List<UserLog> customerLogs = FileManager.getCustomerLog(accNr);
		Collections.reverse(customerLogs);

		String type = "";
		String amount = "";
		String balance = "";
		String otherName = "";
		String otherNr = "";
		String date = "";

		String divide = " ";
		for (int i = 0; i < 113; i++) {
			divide += "-";
		}
		divide += "\n";

		String text = divide;

		for (UserLog log : customerLogs) {

			String other = "";
			type = Translation.translateType(log.getLogType());
			amount = String.format("%.2f", log.getAmount());
			balance = String.format(Translation.getText("balance2") + "%.2f", log.getTotalBalance());
			date = log.getLogCreationTime();
			try {
				otherName = BankDatabase.accounts.get(log.getOtherAccount()).getUsername();
			} catch (NullPointerException e) {
				otherName = Translation.getText("deletedUser");
			}
			otherNr = log.getOtherAccount();
			if (!otherNr.equals(accNr)) {
				if (log.getAmount() > 0) {
					other = "  " + Translation.getText("from") + otherName + " ( " + otherNr + " )";
				} else {
					other = "  " + Translation.getText("to") + otherName + " ( " + otherNr + " )";
				}
			}
			text += " " + date + " - " + type + ":  " + amount + other + "  " + balance + "\n";
			text += divide;
		}
		return text;
	}

	/**
	 * 
	 * @return Kontonummer des Transaktionspartners
	 */
	public String getOtherAccount() {
		return otherAccount;
	}

	/**
	 * 
	 * @param accNr Kontonummer des Transaktionspartners
	 */
	public void setOtherAccount(String accNr) {
		this.otherAccount = accNr;
	}

	/**
	 * 
	 * @return Betrag
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * 
	 * @param amount Betrag
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}

	/**
	 * 
	 * @return Kontostand
	 */
	public double getTotalBalance() {
		return totalBalance;
	}

	/**
	 * 
	 * @param newTotalBalance Kontostand
	 */
	public void setTotalBalance(double newTotalBalance) {
		this.totalBalance = newTotalBalance;
	}

	/**
	 * 
	 * @return Transaktionstyp
	 */
	public LogTypes getLogType() {
		return logType;
	}

	/**
	 * 
	 * @param logType Transaktionstyp
	 */
	public void setLogType(LogTypes logType) {
		this.logType = logType;
	}

	/**
	 * 
	 * @return Zeitstempel
	 */
	public String getLogCreationTime() {
		return logCreationTime;
	}

	/**
	 * 
	 * @param logCreationTime Zeitstempel
	 */
	public void setLogCreationTime(String logCreationTime) {
		this.logCreationTime = logCreationTime;
	}
}
