import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

/**
 * Diese Klasse verwaltet alle Dateizugriffe
 * 
 * @author Elijah Seyfarth
 *
 */
public class FileManager {
	public static final String userDirectory = System.getProperty("user.dir");
	private static final String usersFilePath = userDirectory + "/JsonFiles/users.json";
	private static final String userLogFilePath = userDirectory + "/JsonFiles/userLog.json";
	private static final String adminLogFilePath = userDirectory + "/JsonFiles/adminLog.json";

	/**
	 * Diese Methode liest alle Accounts aus der Nutzerdatei aus und gibt diese in
	 * Form einer Liste zur�ck
	 * 
	 * @return Liste an Accounts
	 */
	public static List<Account> getAccountList() {

		Gson gson = new Gson();
		StringBuilder content = getJsonFileContent(usersFilePath);
		JSONObject wholeJsonObject = new JSONObject(content.toString());
		JSONObject users = wholeJsonObject.getJSONObject("users");
		JSONArray accountArray = new JSONArray();
		Iterator<String> keys = users.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			accountArray.put(users.get(key));
		}
		List<Account> accountList = gson.fromJson(String.valueOf(accountArray), new TypeToken<List<Account>>() {
		}.getType());
		return accountList;
	}

	/**
	 * Diese Methode liest die Accountdaten eines einzelnen Accounts aus
	 * 
	 * @param accountNumber Kontonummer
	 * @return Account
	 */
	public static Account refreshAccountData(String accountNumber) {

		StringBuilder content = getJsonFileContent(usersFilePath);
		JSONObject wholeJsonObject = new JSONObject(content.toString());
		JSONObject users = wholeJsonObject.getJSONObject("users");
		Gson gson = new Gson();
		Account account = gson.fromJson(String.valueOf(users.get(accountNumber)), new TypeToken<Account>() {
		}.getType());
		return account;
	}

	/**
	 * Diese Methode gibt die aktuell n�chste freie Kontonummer zur�ck
	 * 
	 * @param admin Adminrechte
	 * @return Kntonummer
	 */
	public static String getNewAccountNumber(boolean admin) {

		StringBuilder userFile = getJsonFileContent(usersFilePath);
		JSONObject wholeJsonObject = new JSONObject(userFile.toString());
		JSONObject newAccountNumbers = wholeJsonObject.getJSONObject("newAccountNumbers");
		if (admin) {
			return Integer.toString(newAccountNumbers.getInt("adminAccount"));
		} else {
			return Integer.toString(newAccountNumbers.getInt("userAccount"));
		}
	}

	/**
	 * Diese Methode legt in der Datenbank einen neuen Account an und aktualisiert
	 * die n�chste freie Kontonummer
	 * 
	 * @param account Account
	 * @param admin   Adminrechte
	 */
	public static void addAccountInJsonFile(Account account, boolean admin) {

		// Add Account to Users
		StringBuilder userFile = getJsonFileContent(usersFilePath);
		JSONObject wholeJsonObject = new JSONObject(userFile.toString());
		JSONObject users = wholeJsonObject.getJSONObject("users");
		JSONObject newAccount = new JSONObject(account);
		users.put(getNewAccountNumber(admin), newAccount);
		wholeJsonObject.put("users", users);
		String formattedJson = formatJsonObject(wholeJsonObject);
		saveChangesInJsonFile(formattedJson, usersFilePath);

		// Increment new Accountnumber
		JSONObject newAccountNumbers = wholeJsonObject.getJSONObject("newAccountNumbers");
		if (admin) {
			int adminNr = newAccountNumbers.getInt("adminAccount");
			newAccountNumbers.put("adminAccount", ++adminNr);
		} else {
			int userNr = newAccountNumbers.getInt("userAccount");
			newAccountNumbers.put("userAccount", ++userNr);
		}
		wholeJsonObject.put("newAccountNumbers", newAccountNumbers);
		formattedJson = formatJsonObject(wholeJsonObject);
		saveChangesInJsonFile(formattedJson, usersFilePath);

		// Create new Log Object for new Account
		if (!admin) {
			StringBuilder logFile = getJsonFileContent(userLogFilePath);
			JSONObject wholeLogObject = new JSONObject(logFile.toString());
			JSONArray newUser = new JSONArray();
			wholeLogObject.put(account.getAccountNumber(), newUser);

			formattedJson = formatJsonObject(wholeLogObject);
			saveChangesInJsonFile(formattedJson, userLogFilePath);
		}
	}

	/**
	 * Diese Methode l�scht einen Account aus der Datenbank
	 * 
	 * @param accNr Kontonummer
	 */
	public static void deleteAccountFromJsonFile(String accNr) {

		StringBuilder content = getJsonFileContent(usersFilePath);
		JSONObject wholeJsonObject = new JSONObject(content.toString());
		JSONObject users = wholeJsonObject.getJSONObject("users");
		users.remove(accNr);
		wholeJsonObject.put("users", users);
		String formattedJsonWithNewChanges = formatJsonObject(wholeJsonObject);
		saveChangesInJsonFile(formattedJsonWithNewChanges, usersFilePath);
	}

	/**
	 * Diese Methode ver�ndert den Kontostand eines einzelnen Nutzers um den
	 * angegebenen Betrag
	 * 
	 * @param accNr  Kontonummer
	 * @param amount Betrag
	 */
	public static void modifyBalance(String accNr, double amount) {

		StringBuilder content = getJsonFileContent(usersFilePath);
		JSONObject wholeJsonObject = new JSONObject(content.toString());
		JSONObject users = wholeJsonObject.getJSONObject("users");
		JSONObject accountObject = users.getJSONObject(accNr);

		Account account = BankDatabase.getAccount(accNr);
		accountObject.put("totalBalance", account.getTotalBalance() + amount);
		users.put(accNr, accountObject);
		wholeJsonObject.put("users", users);
		String formattedJsonWithNewChanges = formatJsonObject(wholeJsonObject);
		saveChangesInJsonFile(formattedJsonWithNewChanges, usersFilePath);
	}

	/**
	 * Diese Methode liest alle Logeintr�ge eines einzelnen Nutzers aus und gibt
	 * diese in Form einer Liste zur�ck
	 * 
	 * @param accountNumber Kontonummer
	 * @return Liste an Nutzer-Logeintr�gen
	 */
	public static List<UserLog> getCustomerLog(String accountNumber) {
		StringBuilder content = getJsonFileContent(userLogFilePath);
		JSONObject wholeJsonObject = new JSONObject(content.toString());
		JSONArray userLogs = wholeJsonObject.getJSONArray(accountNumber);

		Gson gson = new Gson();
		List<UserLog> logList = gson.fromJson(String.valueOf(userLogs), new TypeToken<List<UserLog>>() {
		}.getType());

		return logList;
	}

	/**
	 * Diese Methode liest alle Logeintr�ge der Administratoren aus und gibt diese
	 * in Form einer Liste zur�ck
	 * 
	 * @return Liste an Admin-Logeintr�gen
	 */
	public static List<AdminLog> getAdminLog() {
		StringBuilder content = getJsonFileContent(adminLogFilePath);
		JSONObject wholeJsonObject = new JSONObject(content.toString());
		JSONArray adminLogs = wholeJsonObject.getJSONArray("admin");

		Gson gson = new Gson();
		List<AdminLog> logList = gson.fromJson(String.valueOf(adminLogs), new TypeToken<List<AdminLog>>() {
		}.getType());

		return logList;
	}

	/**
	 * Diese Methode erstellt einen Logeintrag f�r eine Transaktion
	 * 
	 * @param accNr   Kontonummer
	 * @param accNr2  Kontonummer des Transaktionspartners
	 * @param logType Transaktionstyp
	 * @param amount  Betrag
	 */
	public static void logTransaction(String accNr, String accNr2, LogTypes logType, double amount) {

		StringBuilder content = getJsonFileContent(userLogFilePath);
		JSONObject wholeJsonObject = new JSONObject(content.toString());
		JSONArray userLogs = wholeJsonObject.getJSONArray(accNr);
		ZonedDateTime time = Instant.now().atZone(ZoneId.of("Europe/Berlin"));
		String logCreationTime = time.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
		JSONObject newLogObject = new JSONObject();

		newLogObject.put("logCreationTime", logCreationTime);
		newLogObject.put("logType", logType.name());
		newLogObject.put("amount", amount);
		newLogObject.put("totalBalance", BankDatabase.getAccount(accNr).getTotalBalance());
		newLogObject.put("otherAccount", accNr2);

		userLogs.put(newLogObject);
		wholeJsonObject.put(accNr, userLogs);
		String formattedJson = formatJsonObject(wholeJsonObject);
		saveChangesInJsonFile(formattedJson, userLogFilePath);
	}

	/**
	 * Diese Methode erstellt einen Logeintrag einer Admin-Aktion
	 * 
	 * @param accNr   Kontonummer
	 * @param logType Transaktionstyp
	 */
	public static void logAdminOperation(String accNr, LogTypes logType) {
		StringBuilder content = getJsonFileContent(adminLogFilePath);
		JSONObject wholeJsonObject = new JSONObject(content.toString());
		JSONArray adminLog = wholeJsonObject.getJSONArray("admin");

		ZonedDateTime time = Instant.now().atZone(ZoneId.of("Europe/Berlin"));
		String logCreationTime = time.format(DateTimeFormatter.ofPattern("dd.MM.YYYY HH:mm:ss"));
		JSONObject newLogObject = new JSONObject();

		newLogObject.put("logCreationTime", logCreationTime);
		newLogObject.put("logType", logType.name());
		newLogObject.put("accountNumber", BankDatabase.getAccount(accNr).getAccountNumber());

		adminLog.put(newLogObject);
		wholeJsonObject.put("admin", adminLog);
		String formattedJson = formatJsonObject(wholeJsonObject);
		saveChangesInJsonFile(formattedJson, adminLogFilePath);
	}

	/**
	 * Diese Methode schreibt den angegebenen String in den angegebenen Dateipfad
	 * 
	 * @param jsonString String
	 * @param filePath   Dateipfad
	 */
	public static void saveChangesInJsonFile(String jsonString, String filePath) {

		try {
			FileWriter file = new FileWriter(filePath);
			file.write(jsonString);
			file.flush();
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Diese Methode formatiert das angegebene Json-Objekt
	 * 
	 * @param object Json-Objekt
	 * @return formatierter String
	 */
	private static String formatJsonObject(JSONObject object) {

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonParser parser = new JsonParser();
		JsonObject json = parser.parse(object.toString()).getAsJsonObject();

		return gson.toJson(json);
	}

	/**
	 * Diese Methode liest einen String aus dem angegebenen Dateipfad
	 * 
	 * @param filePath Dateipfad
	 * @return Stringbuilder
	 */
	public static StringBuilder getJsonFileContent(String filePath) {

		String path = String.valueOf(FileSystems.getDefault().getPath(filePath).toAbsolutePath());
		StringBuilder contentBuilder = new StringBuilder();

		try (Stream<String> stream = Files.lines(Path.of(path), StandardCharsets.UTF_8)) {
			stream.forEach(s -> contentBuilder.append(s).append("\n"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return contentBuilder;
	}

	/**
	 * Diese Methode initialisiert die Nutzerdatei mit einem Admin-Account und
	 * erstellt die n�chsten freien Kontonummern
	 * 
	 * @param admin Admin-Account
	 */
	public static void initializeUserFile(Account admin) {

		createFile(usersFilePath);
		JSONObject wholeJsonObject = new JSONObject();
		JSONObject users = new JSONObject();
		JSONObject acc0 = new JSONObject(admin);
		users.put("99000001", acc0);
		JSONObject newAccountNumbers = new JSONObject();
		newAccountNumbers.put("userAccount", 11000001);
		newAccountNumbers.put("adminAccount", 99000002);
		wholeJsonObject.put("newAccountNumbers", newAccountNumbers);
		wholeJsonObject.put("users", users);
		saveChangesInJsonFile(formatJsonObject(wholeJsonObject), usersFilePath);
	}

	/**
	 * Diese Methode initialisiert die Log-Dateien
	 */
	public static void initializeLogFiles() {

		createFile(adminLogFilePath);
		createFile(userLogFilePath);
		JSONObject wholeJsonObject = new JSONObject();
		JSONArray log0 = new JSONArray();
		wholeJsonObject.put("admin", log0);
		saveChangesInJsonFile(formatJsonObject(wholeJsonObject), adminLogFilePath);

		wholeJsonObject = new JSONObject();
		saveChangesInJsonFile(formatJsonObject(wholeJsonObject), userLogFilePath);
	}

	/**
	 * Diese Methode erstellt eine neue Datei und alle Verzeichnisse des angegebenen
	 * Dateipfads
	 * 
	 * @param filepath Dateipfad
	 */
	public static void createFile(String filepath) {
		File f = new File(filepath);
		f.getParentFile().mkdirs();
		f.delete();
		try {
			f.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
